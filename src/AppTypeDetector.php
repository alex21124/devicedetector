<?php

namespace Skarbgroup\Devdetec;

use Skarbgroup\Devdetec\Mobile_Detect;

class AppTypeDetector {
    
	public static function getType() {
        $detector = new Mobile_Detect();
        if ( $detector->isMobile() && !$detector->isTablet() ) {
            return 'mobile';
        }
        return 'desktop';		
	}
}

?>
